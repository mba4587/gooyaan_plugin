import 'dart:io';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:image_picker/image_picker.dart';
import 'package:zefyr_plus/zefyr.dart';

/// Custom image delegate used by this example to load image from application
/// assets.
class CustomImageDelegate implements ZefyrImageDelegate<ImageSource> {
  CustomImageDelegate({this.onImageClick});

  @override
  ImageSource get cameraSource => ImageSource.camera;

  @override
  ImageSource get gallerySource => ImageSource.gallery;

  final Function(String url) onImageClick;

  @override
  Widget buildImage(BuildContext context, String key) {
    List keyList = [];
    List<Widget> imageList = [];
    if (keyList.length > 0) {
      for (int i = 0; i < keyList.length; i++) {
        var asset = CachedNetworkImageProvider(keyList[i]);
        imageList.add(GestureDetector(
          child: Image(
            image: asset,
            fit: BoxFit.fitHeight,
            width: MediaQuery.of(context).size.width,
          ),
          onTap: () {
            if (onImageClick != null) onImageClick(key);
          },
        ));
      }
      return CarouselSlider(
          items: imageList,
          options: CarouselOptions(
            height: 400,
            aspectRatio: 16 / 9,
            viewportFraction: 0.8,
            initialPage: 0,
            enableInfiniteScroll: true,
            reverse: false,
            autoPlay: true,
            autoPlayInterval: Duration(seconds: 3),
            autoPlayAnimationDuration: Duration(milliseconds: 800),
            autoPlayCurve: Curves.fastOutSlowIn,
            enlargeCenterPage: true,
            scrollDirection: Axis.horizontal,
          ));
    } else {
      // We use custom "asset" scheme to distinguish asset images from other files.
      if (key.startsWith('asset://')) {
        final asset = AssetImage(key.replaceFirst('asset://', ''));
        return GestureDetector(child: Image(image: asset));
      } else if (key.startsWith("http://") || key.startsWith("https://")) {
        final asset = CachedNetworkImageProvider(key);
        return GestureDetector(
          child: Image(
            image: asset,
            fit: BoxFit.fitHeight,
            width: MediaQuery.of(context).size.width,
          ),
          onTap: () {
            if (onImageClick != null) onImageClick(key);
          },
        );
      } else {
        // Otherwise assume this is a file stored locally on user's device.
        final file = File.fromUri(Uri.parse(key));
        final image = FileImage(file);
        return Image(image: image);
      }
    }
  }

  @override
  Future<String> pickImage(ImageSource source) {
    // TODO: implement pickImage
    throw UnimplementedError();
  }
}
