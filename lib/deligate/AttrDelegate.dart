import 'package:gooyaan/constants/config.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:zefyr_plus/zefyr.dart';

/// Custom image delegate used by this example to load image from application
/// assets.
class CustomAttrDelegate implements ZefyrAttrDelegate {
  final String postId, userId;

  CustomAttrDelegate(this.postId, this.userId);

  @override
  void onLinkTap(String link) async {
    if (await canLaunch(ConfigConstants.moveURL +
        link +
        "&post_id=" +
        postId +
        "&user_id=" +
        userId)) {
      launch(ConfigConstants.moveURL +
          link +
          "&post_id=" +
          postId +
          "&user_id=" +
          userId);
    }
  }
}
