import 'package:chewie/chewie.dart';
import 'package:flare_flutter/flare_actor.dart';
import 'package:flutter/material.dart';
import 'package:gooyaan/constants/package_config.dart';
import 'package:video_player/video_player.dart';
import 'package:zefyr_plus/zefyr.dart';

class CustomVideoDelegate implements ZefyrVideoDelegate {
  final BuildContext context;
  final bool showThumbnail;
  final bool showAd;
  final String adId;
  final String adImage;
  final Function(bool videoIsLoaded) isLoadedVideo;
  VideoPlayerController videoPlayerController;
  ChewieController chewieController;
  String key;

  CustomVideoDelegate(this.showThumbnail, this.context,
      {this.isLoadedVideo, this.adId, this.adImage, this.showAd = false});

  bool isLoaded = false;

//  bool isAdLoaded = true;
//  int timer = 3;
  @override
  initState(String k, VoidCallback setState) {
    this.key = k;
    videoPlayerController = VideoPlayerController.network(key);
    chewieController = ChewieController(
        videoPlayerController: videoPlayerController,
        aspectRatio: 3 / 2,
        errorBuilder: (context, errorMessage) {
          return Center(
            child: Icon(Icons.error),
          );
        },
        autoPlay: true,
        allowMuting: true,
        showControls: !showThumbnail,
        overlay: Padding(
          padding: EdgeInsets.symmetric(vertical: 8, horizontal: 16),
          child: Container(
            width: 60,
            height: 60,
            child: FlareActor(
                PackageConfig.PACKAGE_ADDRESS + "assets/flare/gooyaan.flr",
                alignment: Alignment.center,
                animation: "Alarm"),
          ),
        ),
        placeholder: (showAd && adImage != null)
            ? Center(
                child: Stack(
                  children: [
                    GestureDetector(
                      onTap: () {},
                      child: Image(
                        image: NetworkImage(adImage),
                      ),
                    ),
                    Positioned(
                      bottom: 20,
                      right: 0,
                      child: Container(
                          padding:
                              EdgeInsets.symmetric(vertical: 8, horizontal: 16),
                          decoration: BoxDecoration(
                            color: Colors.black38,
                          ),
                          child: CircularProgressIndicator()),
                    ),
                  ],
                ),
              )
            : Center(child: Icon(Icons.ondemand_video)),
        looping: false,
        showControlsOnInitialize: !showThumbnail);
    videoPlayerController.addListener(() {
      if (videoPlayerController.value.isPlaying && isLoaded == false) {
        videoPlayerController.pause();
        chewieController.pause();
        isLoaded = true;
        isLoadedVideo(isLoaded);
        setState();
      }
    });
//        if(showAd && isLoaded) {
//          Timer.periodic(Duration(seconds: 1), (v) {
//            if (timer <= 0) {
//              isAdLoaded = false;
//              v.cancel();
//            } else {
//              timer--;
//            }
//            print(timer);
//            setState();
//          });
//        }
  }

  @override
  Widget buildVideo(BuildContext context) {
    if (key.endsWith(".mkv") ||
        key.endsWith(".mp4") ||
        key.endsWith(".wav") ||
        key.endsWith(".mp3")) {
      final playerWidget = Chewie(
        controller: chewieController,
      );
      return playerWidget;
    } else {
      return Center(
        child: Text("ویدئو یافت نشد."),
      );
    }
  }

  @override
  void dispose() {
    chewieController?.dispose();
    videoPlayerController?.dispose();
  }
}
