import 'package:flutter/material.dart';

mixin WidgetLoadMixin<T extends StatefulWidget> on State<T> {
  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((_) => onLoad(context));
  }

  void onLoad(BuildContext context);
}
