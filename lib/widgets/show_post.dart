import 'dart:convert';

import 'package:apex_flutter_plugin/api/api.dart';
import 'package:apex_flutter_plugin/api/base_request.dart';
import 'package:apex_flutter_plugin/api/request.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:gooyaan/constants/config.dart';
import 'package:gooyaan/constants/package_config.dart';
import 'package:gooyaan/deligate/AttrDelegate.dart';
import 'package:gooyaan/deligate/CustomImageDeligate.dart';
import 'package:gooyaan/deligate/CustomVideoDeligate.dart';
import 'package:gooyaan/models/post.dart';
import 'package:gooyaan/models/user.dart';
import 'package:gooyaan/utils/storage_util.dart';
import 'package:gooyaan/utils/widget_load.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:zefyr_plus/zefyr.dart';
import 'package:quill_delta/quill_delta.dart';

/// The Gooyaan article view template (Dark / Light)
enum ArticleStyle { dark, light }

/// Gooyaan Article view, just send your api_key and post id and get the article
class ArticleView extends StatefulWidget {
  /// The post id you've got from (getArticles())
  final String postId;

  /// The api key you've got from gooyaan android application.
  final apiKey;

  /// What do you want to do after article loaded (you have access to the post property but you can't change it on this widget)
  final Function(PostModel post) onPrepared;

  /// Show the writer of the post and the count of likes
  final bool showFullPreview;

  /// Article style (you can change it between dark and light)
  final ArticleStyle articleStyle;

  /// You have access to transfer user in to a page and show the picture on tapped by user.
  final Function(String url) onImageTap;

  const ArticleView(
      {Key key,
      this.postId,
      this.apiKey,
      this.onPrepared,
      this.articleStyle = ArticleStyle.dark,
      this.showFullPreview = false,
      this.onImageTap})
      : super(key: key);

  @override
  _ArticleViewState createState() => _ArticleViewState();
}

Delta getDelta(String doc) {
  return Delta.fromJson(json.decode(utf8.decode(base64Decode(doc))) as List);
}

class _ArticleViewState extends State<ArticleView> with WidgetLoadMixin {
  bool isLoaded = false;
  String message = "درحال بارگذاری";
  PostModel post;
  NotusDocument doc;
  String adImage, adTitle, adDescription, adIcon, adCallToAction = "asd", adId;
  bool adExist = false;
  bool showAdVideo = false;
  double _fontSize =
      StorageUtil.getDouble("gyn_font_size", defValue: 0.0) == 0.0
          ? 16
          : StorageUtil.getDouble("gyn_font_size");
  Color _textColor;

  void getPostData() {
    Api.shared
        .makeRequest(IsolateModel(
            Request({
              "action": ConfigConstants.actionGetPost,
              "post_id": widget.postId,
              "api_key": widget.apiKey,
            }, method: Method.POST),
            url: ConfigConstants.apiUrl,
            encrypt: false))
        .then((response) {
      var resp = response.result;
      if (resp['ok']) {
        var result = resp['result'];
        post = PostModel(
          createdTimeJalali: result['time_created_fa'],
          createdTimeMiladi: result['time_created'],
          title: result['title'],
          shortDescription: result['description'],
          content: result['content'],
          isFree: result['is_free'],
          likes: result['likes'],
          id: result['post_id'],
          from: UserModel(
            bio: result['from']['bio'],
            firstName: result['from']['first_name'],
            lastName: result['from']['last_name'],
            userId: result['from']['user_id'],
            username: result['from']['username'],
            profilePicture: resp['images'][widget.postId]
                [result['from']['profile_image']],
          ),
          imageUrl: resp['images'][widget.postId][result['head_image']],
        );
        if (result['content'] == null) {
          setState(() {
            message = 'مقاله قابل مشاهده نیست';
          });
        } else {
          doc =
              NotusDocument.fromDelta(getDelta(response['result']['content']));
          setState(() {
            isLoaded = true;
          });
          widget.onPrepared(post);
        }
      } else {
        setState(() {
          message = resp['message'];
        });
      }
    });
  }

  @override
  void initState() {
    super.initState();
    setState(() {
      _textColor = widget.articleStyle == ArticleStyle.dark
          ? Colors.white
          : Colors.black;
    });
  }

  @override
  Widget build(BuildContext context) {
    var _fontStyle = TextStyle(
      fontFamily: ConfigConstants.fontFamily,
      package: PackageConfig.PACKAGE_NAME,
      fontSize: _fontSize,
      color: _textColor,
    );
    var _codeStyle = TextStyle(
      fontFamily: ConfigConstants.fontFamily,
      fontSize: _fontSize * 6 / 7,
      decorationStyle: TextDecorationStyle.dashed,
      locale: Locale("en"),
    );
    var _heading3Style = TextStyle(
        fontSize: _fontSize + 6,
        color: widget.articleStyle == ArticleStyle.dark
            ? Colors.white
            : Colors.blue);
    var _heading2Style = TextStyle(
        fontSize: _fontSize + 8,
        color: widget.articleStyle == ArticleStyle.dark
            ? Colors.white
            : Colors.blue);
    var _heading1Style = TextStyle(
        fontSize: _fontSize + 10,
        color: widget.articleStyle == ArticleStyle.dark
            ? Colors.white
            : Colors.blue);
    var _linkStyle = TextStyle(
        color: widget.articleStyle == ArticleStyle.dark
            ? Colors.amberAccent
            : Colors.blue);
    var _quoteStyle = TextStyle(
        color: widget.articleStyle == ArticleStyle.dark
            ? Colors.orange
            : Colors.blue);

    return Scaffold(
      backgroundColor: widget.articleStyle == ArticleStyle.dark
          ? Colors.grey[800]
          : Colors.grey[200],
      body: isLoaded
          ? Directionality(
              textDirection: TextDirection.rtl,
              child: ListView(physics: BouncingScrollPhysics(), children: [
                if (widget.showFullPreview) ...[
                  Container(
                    decoration: BoxDecoration(
                      color: widget.articleStyle == ArticleStyle.dark
                          ? Colors.grey[900]
                          : Colors.white,
                      boxShadow: [
                        BoxShadow(
                            color: Colors.black12,
                            spreadRadius: 0.5,
                            blurRadius: 10)
                      ],
                    ),
                    child: Padding(
                      padding: EdgeInsets.all(16),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Row(
                            children: [
                              CircleAvatar(
                                radius: 30,
                                child: Container(
                                  decoration: BoxDecoration(boxShadow: [
                                    BoxShadow(
                                        color: Colors.black12,
                                        spreadRadius: 0.5,
                                        blurRadius: 10)
                                  ], borderRadius: BorderRadius.circular(100)),
                                  child: ClipOval(
                                    child: Image(
                                      image: CachedNetworkImageProvider(
                                          post.from.profilePicture),
                                    ),
                                  ),
                                ),
                              ),
                              SizedBox(
                                width: 16,
                              ),
                              Column(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text(
                                      post.from.firstName +
                                          ' ' +
                                          post.from.lastName,
                                      style: TextStyle(
                                          fontSize: 18,
                                          color: widget.articleStyle ==
                                                  ArticleStyle.dark
                                              ? Colors.white
                                              : Colors.black,
                                          fontFamily:
                                              ConfigConstants.fontFamily,
                                          package: PackageConfig.PACKAGE_NAME),
                                    ),
                                    Text(
                                      post.from.username,
                                      style: TextStyle(
                                          fontSize: 14,
                                          color: widget.articleStyle ==
                                                  ArticleStyle.dark
                                              ? Colors.orange
                                              : Colors.blue,
                                          fontFamily:
                                              ConfigConstants.fontFamily,
                                          package: PackageConfig.PACKAGE_NAME),
                                    )
                                  ])
                            ],
                          ),
                          Row(
                            children: [
                              Text(
                                post.likes,
                                style: TextStyle(
                                    fontSize: 14,
                                    color:
                                        widget.articleStyle == ArticleStyle.dark
                                            ? Colors.white
                                            : Colors.black,
                                    fontFamily: ConfigConstants.fontFamily,
                                    package: PackageConfig.PACKAGE_NAME),
                              ),
                              SizedBox(
                                width: 8,
                              ),
                              Icon(
                                Icons.favorite,
                                color: Colors.pink,
                              ),
                            ],
                          )
                        ],
                      ),
                    ),
                  ),
                ],
                Padding(
                  padding: EdgeInsets.only(left: 16, bottom: 16, right: 16),
                  child: ZefyrTheme(
                    data: ZefyrThemeData(
                        attributeTheme: AttributeTheme(
                            bold: _fontStyle,
                            italic: _fontStyle,
                            numberList: BlockTheme(textStyle: _fontStyle),
                            bulletList: BlockTheme(textStyle: _fontStyle),
                            heading3: LineTheme(
                              textStyle: _heading3Style,
                              padding: EdgeInsets.only(top: 10, bottom: 5),
                            ),
                            heading2: LineTheme(
                              textStyle: _heading2Style,
                              padding: EdgeInsets.only(top: 10, bottom: 5),
                            ),
                            heading1: LineTheme(
                              textStyle: _heading1Style,
                              padding: EdgeInsets.only(top: 10, bottom: 5),
                            ),
                            quote: BlockTheme(
                              textStyle: _quoteStyle,
                            ),
                            link: _linkStyle,
                            code: BlockTheme(textStyle: _codeStyle)),
                        indentWidth: 16,
                        defaultLineTheme: LineTheme(
                          textStyle: _fontStyle,
                          padding: EdgeInsets.only(top: 10, bottom: 5),
                        )),
                    child: ZefyrView(
                      document: doc,
                      attrDelegate:
                          CustomAttrDelegate(post.id, post.from.userId),
                      imageDelegate: CustomImageDelegate(onImageClick: (url) {
                        widget.onImageTap(url);
                      }),
                      videoDelegate: CustomVideoDelegate(false, context,
                          showAd: false, adId: adId, adImage: adImage,
                          isLoadedVideo: (bool isLoadedVideo) {
                        if (isLoadedVideo && showAdVideo) {
                          setState(() {
                            showAdVideo = false;
                          });
                        }
                      }),
                    ),
                  ),
                ),
                Container(
                  decoration: BoxDecoration(
                    color: widget.articleStyle == ArticleStyle.dark
                        ? Colors.grey[900]
                        : Colors.white,
                    boxShadow: [
                      BoxShadow(
                          color: Colors.black12,
                          spreadRadius: 0.5,
                          blurRadius: 10)
                    ],
                  ),
                  child: Padding(
                    padding: EdgeInsets.all(16),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(post.createdTimeJalali,
                            style: TextStyle(
                                color: widget.articleStyle == ArticleStyle.dark
                                    ? Colors.white
                                    : Colors.black,
                                fontFamily: ConfigConstants.fontFamily,
                                package: PackageConfig.PACKAGE_NAME)),
                        GestureDetector(
                          onTap: () {
                            if (canLaunch(ConfigConstants.website) != null) {
                              launch(ConfigConstants.website);
                            }
                          },
                          child: Row(
                            children: [
                              Text("قدرت گرفته از گویان",
                                  style: TextStyle(
                                      color: widget.articleStyle ==
                                              ArticleStyle.dark
                                          ? Colors.white
                                          : Colors.black,
                                      fontFamily: ConfigConstants.fontFamily,
                                      package: PackageConfig.PACKAGE_NAME)),
                              SizedBox(width: 10),
                              Image(
                                width: 30,
                                height: 30,
                                image: CachedNetworkImageProvider(
                                    "https://gooyaan.ir/images/gooyan.png"),
                              ),
                            ],
                          ),
                        )
                      ],
                    ),
                  ),
                ),
              ]),
            )
          : Center(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  CircularProgressIndicator(),
                  SizedBox(
                    height: 30,
                  ),
                  Text(message,
                      style: TextStyle(
                          color: _textColor,
                          fontFamily: ConfigConstants.fontFamily,
                          package: PackageConfig.PACKAGE_NAME))
                ],
              ),
            ),
    );
  }

  @override
  void onLoad(BuildContext context) {
    getPostData();
  }
}
