class ConfigConstants {
  static final String apiUrl = "https://gooyaan.ir/developer/api/index.php";
  static final String actionGetPost = 'getPost';
  static final String actionGetUserInfoAndPosts = 'getUser';
  static final String moveURL = "https://gooyaan.ir/move/?url=";
  static final String downloadUrl = 'https://dl.gooyaan.ir/latest';
  static final String fontFamily = 'IRANSans';
  static final String website = 'https://gooyaan.ir/';
}
