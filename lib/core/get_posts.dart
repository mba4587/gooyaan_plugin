import 'package:apex_flutter_plugin/api/api.dart';
import 'package:apex_flutter_plugin/api/request.dart';
import 'package:apex_flutter_plugin/api/response.dart';
import 'package:flutter/cupertino.dart';
import 'package:gooyaan/constants/config.dart';
import 'package:gooyaan/models/post.dart';
import 'package:gooyaan/models/user.dart';

/// get list of posts you shared on gooyaan application (only posted not drafted) with your own api key
Future<List<PostModel>> getArticles({@required String apiKey}) async {
  List<PostModel> posts = List();
  Response response = await Api().makeRequest(IsolateModel(
      Request({
        "action": ConfigConstants.actionGetUserInfoAndPosts,
        "api_key": apiKey
      }),
      url: ConfigConstants.apiUrl,
      encrypt: false));
  var resp = response.result;
  var user = resp['user'];
  final userModel = UserModel(
    username: user['username'],
    userId: user['user_id'],
    profilePicture: resp['images'][user['user_id']][user['profile_image']],
    bio: user['bio'],
    firstName: user['first_name'],
    lastName: user['last_name'],
  );

  for (int i = 0; i < (resp['posts'] as List).length; i++) {
    var post = resp['posts'][i];
    posts.add(PostModel(
      title: post['title'],
      shortDescription: post['description'],
      imageUrl: resp['images'][post['post_id']][post['head_image']],
      createdTimeMiladi: post['time_created'],
      createdTimeJalali: post['time_created_fa'],
      id: post['post_id'],
      isFree: post['is_free'],
      from: userModel,
    ));
  }
  return posts;
}
