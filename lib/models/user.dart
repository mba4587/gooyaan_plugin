class UserModel {
  final String firstName, lastName, bio, profilePicture, username, userId;

  UserModel(
      {this.username,
      this.userId,
      this.firstName,
      this.lastName,
      this.bio,
      this.profilePicture});

  @override
  String toString() {
    return "username:$username, userId:$userId, firstName:$firstName, lastName:$lastName, bio:$bio, profilePicture:$profilePicture";
  }
}
