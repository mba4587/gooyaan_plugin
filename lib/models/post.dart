import 'package:gooyaan/models/user.dart';

/// Post model for every single post
class PostModel {
  /// The Post id
  final String id;

  /// The Post title
  final String title;

  /// The post description
  final String shortDescription;

  /// The post head image url
  final String imageUrl;

  /// The post type (is free or subscribed)
  final String isFree;

  /// Json Content for display the post
  final content;

  /// User Model for get the post owner
  final UserModel from;

  /// Count of likes
  final String likes;

  /// Time created (Jalali)
  final String createdTimeJalali;

  /// Time created (Miladi)
  final String createdTimeMiladi;

  PostModel(
      {this.createdTimeJalali,
      this.createdTimeMiladi,
      this.likes,
      this.title,
      this.shortDescription,
      this.imageUrl,
      this.id,
      this.isFree,
      this.from,
      this.content});

  @override
  String toString() {
    return 'postId:$id, createdTimeJalali:$createdTimeJalali, createdTimeMiladi:$createdTimeMiladi, likes:$likes, title:$title,shortDescription:$shortDescription,imageUrl:$imageUrl, isFree:$isFree, from:$from, content:$content';
  }
}
