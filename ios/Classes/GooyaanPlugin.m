#import "GooyaanPlugin.h"
#if __has_include(<gooyaan/gooyaan-Swift.h>)
#import <gooyaan/gooyaan-Swift.h>
#else
// Support project import fallback if the generated compatibility header
// is not copied when this plugin is created as a library.
// https://forums.swift.org/t/swift-static-libraries-dont-copy-generated-objective-c-header/19816
#import "gooyaan-Swift.h"
#endif

@implementation GooyaanPlugin
+ (void)registerWithRegistrar:(NSObject<FlutterPluginRegistrar>*)registrar {
  [SwiftGooyaanPlugin registerWithRegistrar:registrar];
}
@end
