import 'package:flutter/material.dart';
import 'package:gooyaan/gooyaan.dart';
import 'package:gooyaan/models/post.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  List<PostModel> list = [];
  String postId = '7876758331';
  String _title = "";
  Widget postWidget;

  @override
  void initState() {
    super.initState();
    _getPosts();
    postWidget = articleWidget(postId);
  }

  Widget articleWidget(postId) {
    return ArticleView(
      apiKey: "161236768827-K1o-ZRa8b8",
      postId: postId,
      showFullPreview: true,
      articleStyle: ArticleStyle.dark,
      onPrepared: (post) {
        setState(() {
          _title = post.title;
        });
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(brightness: Brightness.light),
      home: Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.grey[900],
          title: Text(_title),
        ),
        body: Center(
          child: Column(
            children: [
              SizedBox(
                height: 16,
              ),
              SingleChildScrollView(
                physics: BouncingScrollPhysics(),
                scrollDirection: Axis.horizontal,
                child: Row(
                  children: [
                    for (PostModel x in list) ...[
                      SizedBox(
                        width: 8.0,
                      ),
                      InkWell(
                        borderRadius: BorderRadius.circular(16.0),
                        onTap: () {
                          setState(() {
                            postId = x.id;
                          });
                        },
                        child: Padding(
                          padding: EdgeInsets.all(8.0),
                          child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Container(
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(16.0),
                                    boxShadow: [
                                      BoxShadow(
                                          color: Colors.black12,
                                          blurRadius: 10,
                                          spreadRadius: 0.5),
                                    ],
                                  ),
                                  child: ClipRRect(
                                    borderRadius: BorderRadius.circular(16.0),
                                    child: Image(
                                      width: 100,
                                      height: 150,
                                      fit: BoxFit.fitHeight,
                                      image: NetworkImage(x.imageUrl),
                                    ),
                                  ),
                                ),
                                Container(
                                    width: 100,
                                    child: Text(
                                      x.title,
                                      overflow: TextOverflow.ellipsis,
                                      maxLines: 1,
                                    ))
                              ]),
                        ),
                      ),
                      SizedBox(
                        width: 8.0,
                      ),
                    ],
                  ],
                ),
              ),
              SizedBox(
                height: 16,
              ),
              Expanded(
                child: postWidget,
              ),
            ],
          ),
        ),
      ),
    );
  }

  void _getPosts() async {
    final x = await getArticles(apiKey: "161236768827-K1o-ZRa8b8");
    setState(() {
      for (int i = 0; i < x.length; i++) {
        list.add(x[i]);
      }
    });
  }
}
